const fs = require('fs')
const path = require('path')
const filepath = path.resolve()

function splicer(input) {
    let newArray = []
    for (let i = input.length-1; i >= 0; i--) {
        if (input[i][0] === "P") {
            newArray.push(input.splice(i,input.length))
        }
    }
    newArray.reverse()
    return newArray

}

function xmlPersonWriter (infoArray) {
    let xml 
    switch(infoArray[0]) {
        case 'P':
            
            xml = `\n<firstname>${infoArray[1]}</firstname>` + `\n<lastname>${infoArray[2]}</lastname>`
            return xml      
        
        case 'T':
            return xml = `\n<phone>\n<mobile>${infoArray[1]}</mobile>\n</phone>`
        
                        
        case 'A':
            return xml = `\n<address>\n<street>${infoArray[1]}</street>\n</address>`
            
            
        case 'F':
            xml = `<family>\n<name>${infoArray[1]}</name>`
            if (infoArray[2]) {
                xml = xml + `\n<born>${infoArray[2]}</born>`
            }
            xml = xml + '\n</family>'
            return xml
        }

}  

function personMaker (personArray){
    let cleanInfo
    let personXML = '<person>'

    try {
        fs.writeFileSync((filepath + '/xmltest.txt'), '<people>\n', {flag: 'a'})
        console.log("filewrittensuccesfully")
      } catch (err) {
        console.error(err)
      }

    for (let i = 0; i < personArray.length; i++) {
        cleanInfo = personArray[i].split('\|')
        personXML = personXML + xmlPersonWriter(cleanInfo)
        if (i === personArray.length - 1){
            personXML = personXML + '\n</person>\n'
        }
        try {
            fs.writeFileSync((filepath + '/xmltest.txt'), personXML, {flag: 'a'})
            console.log("filewrittensuccesfully")
          } catch (err) {
            console.error(err)
          }
        personXML = ''
    }

    return personXML;
}


function xmlOutputWriter(data) {
    let xmlOutput = ''
    let personInfo = ''
    let splicerOutput = splicer(data)
    
    for (let i = 0; i < splicerOutput.length; i++) {
       personInfo = personMaker(splicerOutput[i])
       xmlOutput = xmlOutput + personInfo 
    }

    try {
        fs.writeFileSync((filepath + '/xmltest.txt'), '</people>', {flag: 'a'})
        console.log("filewrittensuccesfully")
      } catch (err) {
        console.error(err)
      }

    return xmlOutput
}

function converterCallback (callback) {
    fs.readFile((filepath + '/example.txt'), 'utf8', (err, data ) => {
        if (err) {
            console.error(err)
            return
        }
        let dataString = data.toString()
        let separatedData = dataString.split('\n')
       
        callback(separatedData)
    })
}

converterCallback(xmlOutputWriter)